﻿using System;
using System.Collections.Generic;
using System.Text;
using CS481_HW4.Model;

namespace CS481_HW4.Data
{
    class Data
    {
        public List<Virus> Viruses = new List<Virus>()
        {
            new Virus()
            {
                VirusID = 1,
                Origin = "2020",
                Name = "Corona Virus",
                Description = "Currently ongoing... maybe panic",
                PNG = "corona.jpg"
            },
            new Virus()
            {
                VirusID = 2,
                Origin = "2003",
                Name = "SARS",
                Description = "SARS coronavirus (SARS-CoV) – virus identified in 2003. SARS-CoV is " +
                              "thought to be an animal virus from an as-yet-uncertain animal reservoir, " +
                              "perhaps bats, that spread to other animals (civet cats) and first infected humans " +
                              "in the Guangdong province of southern China in 2002.",
                PNG = "sars.png"
            },
            new Virus()
            {
                VirusID = 3,
                Origin = "2005 - 2012",
                Name = "HIV/AIDS Pandemic",
                Description = "First identified in Democratic Republic of the Congo in 1976, " +
                              "HIV/AIDS has truly proven itself as a global pandemic, killing " +
                              "more than 36 million people since 1981.",
                PNG = "hiv.jpg"
            },
            new Virus()
            {
                VirusID = 4,
                Origin = "1968",
                Name = "Flu Pandemic",
                Description = "A category 2 Flu pandemic sometimes referred to as “the Hong Kong Flu,” the " +
                              "1968 flu pandemic was caused by the H3N2 strain of the Influenza A virus, a " +
                              "genetic offshoot of the H2N2 subtype.",
                PNG = "flu.jpg"
            },
            new Virus()
            {
                VirusID = 4,
                Origin = "1956 - 1958",
                Name = "Asian Flu",
                Description = "Asian Flu was a pandemic outbreak of Influenza A of the H2N2 subtype, " +
                              "that originated in China in 1956 and lasted until 1958. In its two-year spree, " +
                              "Asian Flu traveled from the Chinese province of Guizhou to Singapore, Hong Kong, and the United States.",
                PNG = "asainFlu.jpg"
            },
            new Virus()
            {
                VirusID = 4,
                Origin = "1918",
                Name = "Flu Pandemic",
                Description = "Between 1918 and 1920 a disturbingly deadly outbreak of influenza tore across the globe," +
                              " infecting over a third of the world’s population and ending the lives of 20 – 50 million people.",
                PNG = "flu.jpg"
            },
            new Virus()
            {
                VirusID = 4,
                Origin = "1910 - 1911",
                Name = "Sixth Cholera Pandemic",
                Description = "Like its five previous incarnations, the Sixth Cholera Pandemic originated in India where it " +
                              "killed over 800,000, before spreading to the Middle East, North Africa, Eastern Europe and Russia. ",
                PNG = "cholera.jpg"
            },
            
            new Virus()
            {
                VirusID = 4,
                Origin = "1889 - 1890",
                Name = "Flu Pandemic",
                Description = "Originally the “Asiatic Flu” or “Russian Flu” as it was called, this strain was thought to be an outbreak " +
                              "of the Influenza A virus subtype H2N2, though recent discoveries have instead found the cause to be the " +
                              "Influenza A virus subtype H3N8.",
                PNG = "flu.jpg"
            },
            new Virus()
            {
                VirusID = 4,
                Origin = "1852 - 1860",
                Name = "Third Cholera Pandemic",
                Description = "Generally considered the most deadly of the seven cholera pandemics, the third major outbreak of Cholera in " +
                              "the 19th century lasted from 1852 to 1860.",
                PNG = "cholera.jpg"
            },
            new Virus()
            {
                VirusID = 4,
                Origin = "1346 - 1353",
                Name = "The Black Death",
                Description = "From 1346 to 1353 an outbreak of the Plague ravaged Europe, Africa, and Asia, with an estimated death toll between " +
                              "75 and 200 million people.",
                PNG = "blackDeath.jpg"
            },
            new Virus()
            {
                VirusID = 4,
                Origin = "541 - 542",
                Name = "Plague of Justinian",
                Description = "Thought to have killed perhaps half the population of Europe, the Plague of Justinian was an outbreak of the bubonic plague " +
                              "that afflicted the Byzantine Empire and Mediterranean port cities, killing up to 25 million people in its year long reign of terror.",
                PNG = "justinPlague.jpg"
            },
            new Virus()
            {
                VirusID = 4,
                Origin = "165",
                Name = "Antonine Plague",
                Description = "Also known as the Plague of Galen, the Antonine Plague was an ancient pandemic that affected Asia Minor, Egypt, Greece, and Italy and " +
                              "is thought to have been either Smallpox or Measles, though the true cause is still unknown.",
                PNG = "aPlague.jpg"
            }


        };
    }
}
