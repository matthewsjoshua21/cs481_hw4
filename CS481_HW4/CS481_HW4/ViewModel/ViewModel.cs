﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using CS481_HW4.Model;
using CS481_HW4.View;
using CS481_HW4.ViewModel;
using Xamarin.Forms;

namespace CS481_HW4.ViewModel
{
    public class VirusViewModel
    {
        private ObservableCollection<Virus> _viruses;

        public ObservableCollection<Virus> Viruses
        {
            get => _viruses;
            set => _viruses = value;
        }

        public Command<Virus> Remove
        {
            get { return new Command<Virus>(virus => { Viruses.Remove(virus); }); }

        }
    

        public VirusViewModel()
        {

            Viruses = new ObservableCollection<Virus>();

            Data.Data context = new Data.Data();

            foreach (var virus in context.Viruses)
            {
                Viruses.Add(virus);
            }
        }
    }
}
