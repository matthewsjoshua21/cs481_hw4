﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CS481_HW4.Model
{
    public class Virus
    {
        public int VirusID { get; set; }
        public String Name { get; set; }
        public String Origin { get; set; }
        public String Description { get; set; }
        public String PNG { get; set; }
    }

}
