﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Essentials;


namespace CS481_HW4.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PopUp : ContentPage
    {
        public PopUp(int virusId, String origin, String name, String description, String PNG)
        {
            InitializeComponent();
            GetDesc.Text = description;
            GetImage.Source = PNG;
        }

        private void PopUp_OnAppearing(object sender, EventArgs e)
        {
            UserDialogs.Instance.Toast("Scroll...", TimeSpan.MaxValue);

        }
    }
}