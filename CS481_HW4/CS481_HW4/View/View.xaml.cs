﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS481_HW4.Model;
using CS481_HW4.View;
using CS481_HW4.ViewModel;
using Xamarin.Forms;
using System.Collections.ObjectModel;
using Acr.UserDialogs;

namespace CS481_HW4
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            BindingContext = new VirusViewModel();
        }

        public async void MenuItem_OnClicked(object sender, EventArgs e)
        {
            if (sender is MenuItem index)
            {
                var item = index.BindingContext as Virus;
                if (item != null)
                    await Navigation.PushAsync(new PopUp(item.VirusID, item.Origin, item.Name, item.Description,
                        item.PNG));
            }
        }


        private void ListView_OnRefreshing(object sender, EventArgs e)
        {
            VirusListView.IsRefreshing = false;
        }

        private void MenuItem_OnRemove(object sender, EventArgs e)
        {
            if (sender is MenuItem index)
            {
                if (index.BindingContext is Virus item)
                {
                    if (BindingContext is VirusViewModel list) list.Remove.Execute(item);
                }

            }
        }

        private void VirusListView_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            UserDialogs.Instance.Toast("Long Press to Access the Menu...", TimeSpan.MaxValue);
        }
    }
}
